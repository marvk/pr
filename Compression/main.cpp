#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include "tree.h"
#include "compress.h"
#include "decompress.h"

int symbols = 128;
int readIn = 65536;

struct node **root;

int main(int argc, char **argv) {
    //time measurement
    struct timeval begin, end_compression, end_decompression;

    setbuf(stdout, NULL);
    char filename[2][100];
    strcpy(filename[0], "lorem_ipsum3.txt");
    strcpy(filename[1], "uncompressed.txt");
    for (int i = 1; i < argc; ++i) {
        strcpy(filename[i - 1], argv[i]);
    }

    //create name for compressed output file
    int len = strlen(filename[0]);
    char fileEncoded[100];
    strncpy(fileEncoded, filename[0], len - 3);
    fileEncoded[len - 3] = '\0';
    strcat(fileEncoded, "bin");

    int *letters = new int[symbols]();

    //read data and create huffmann tree
    if (!readData(filename[0], letters)) {
        return 0;
    }

    //printFreq(letters);
    root = createTree(letters);
    struct key_value *binEncoding = new struct key_value[root[0]->count]();
    encoding(binEncoding, root[0]);

    //printTree(root, 1);
    //printEncoding(binEncoding, root[0]->count);

    gettimeofday(&begin, NULL);

    //start actual encoding
    //printf("start compressing ...");
    encodeTextFileParallel(filename[0], fileEncoded, binEncoding);
    //printf("done!\n");

    gettimeofday(&end_compression, NULL);
    double elapsed_compression =
            (end_compression.tv_sec - begin.tv_sec) + ((end_compression.tv_usec - begin.tv_usec) / 1000000.0);
    printf("Runtime compression: %.5fs\n", elapsed_compression);

    /*****************************
    ******************************
    ****PARALLEL BARRIER HERE
    ****Compressing of text file has to be completed,
    ****before decompressing starts
    ****do not compress and decompress at the same time
    ******************************
    *****************************/

    gettimeofday(&begin, NULL);

    //decode
    //printf("start decompressing ...");
    decodeText(fileEncoded, filename[1], root);
    //printf("done!\n");

    gettimeofday(&end_decompression, NULL);
    double elapsed_decompression =
            (end_decompression.tv_sec - begin.tv_sec) + ((end_decompression.tv_usec - begin.tv_usec) / 1000000.0);
    printf("Runtime decompression: %.5fs\n", elapsed_decompression);

    delete[] letters;
    delete[] binEncoding;
}

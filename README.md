# Projects

## Idea

Optimize the serial program for a better speedup. It still has to calculate the
right results. Do not change the algorithm.

Runtime is calculated inside of every program.


## Compile and run
Every folder contains a Makefile, which creates a executable main file.


## Compression
A Huffmann-Tree for encoding a .txt file is used. Time is calculated for
encoding and decoding the file. A different file will be used to calculate the
speedup for your program.


## Game of Life
Conway's Game of Life in 1920x1080 Grid. There is an additional command line
program to convert the resulting images to a playable gif.

Converting the images to a gif is not part of the project. It is just for
entertaining purposes.


## Genetic Algorithm

The longest running program of the three.

The genetic algorithm is not really good in solving the bin-packing-problem,
even a greedy approach may be faster with better results.
However, your main goal is to parallelise the algorithm and not to solve the
bin-packing-problem or text-compression.
